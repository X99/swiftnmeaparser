//
//  NMEAParserTests.swift
//  NMEAParserTests
//
//  Created by X99 on 12/11/2018.
//  Copyright © 2018 X99.fr. All rights reserved.
//

import XCTest
@testable import NMEAParser

class NMEAParserTests: XCTestCase {

    private var correctGPGGASentence = "$GPGGA,123519,4807.038,N,01131.000,E,1,08,0.9,545.4,M,46.9,M,,*47"
    private var incorrectGPGGASentence = "$GPGGA,123519,4807.038,N,01131.000,E,1,08,0.9,545.4,M,46.9,M,,*40"

	private var correctGPGSASentence = "$GPGSA,A,3,04,05,,09,12,,,24,,,,,2.5,1.3,2.1*39"

    override class func setUp() {
        //executed once before all
        
        //let content = String(contentsOfFile: path, encoding: NSUTF8StringEncoding, error: nil)
    }

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testChecksumValidity1() {
        let instance = NMEASentenceParser.NMEASentence(correctGPGGASentence)
        XCTAssertEqual(instance!.checksum(), 0x47)
    }

    func testChecksumValidity2() {
        let instance = NMEASentenceParser.NMEASentence(correctGPGGASentence)
        XCTAssertTrue(instance!.isValid)
    }

    func testChecksumInvalidity1() {
        let instance = NMEASentenceParser.NMEASentence(incorrectGPGGASentence)
        XCTAssertNil(instance)
    }

    func testGPGGAWithValidSentence() {
        if let instance = NMEASentenceParser.shared.parse(correctGPGGASentence) as? NMEASentenceParser.GPGGA {
            XCTAssertEqual(instance.utcTime, 123519)
            XCTAssertEqual(instance.latitude?.coordinate, 4807.038)
            XCTAssertEqual(instance.latitude?.direction, .north)
            XCTAssertEqual(instance.longitude?.coordinate, 1131)
            XCTAssertEqual(instance.longitude?.direction, .east)
            XCTAssertEqual(instance.numberOfSatellites, 8)
            XCTAssertEqual(instance.horizontalDilutionOfPosition, 0.9)
            XCTAssertEqual(instance.mslAltitude, 545.4)
            XCTAssertEqual(instance.mslAltitudeUnit, "M")
            XCTAssertEqual(instance.heightOfGeoid, 46.9)
            XCTAssertEqual(instance.heightOfGeoidUnit, "M")
        } else {
            XCTFail("Failed casting to NMEASentenceParser.GPGGA")
        }
    }
	/*
	$GPGSA,A,3,04,05,,09,12,,,24,,,,,2.5,1.3,2.1*39
	
	Where:
	GSA      Satellite status
	A        Auto selection of 2D or 3D fix (M = manual)
	3        3D fix - values include: 1 = no fix
	2 = 2D fix
	3 = 3D fix
	04,05... PRNs of satellites used for fix (space for 12)
	2.5      PDOP (dilution of precision)
	1.3      Horizontal dilution of precision (HDOP)
	2.1      Vertical dilution of precision (VDOP)
	*39      the checksum data, always begins with *
	*/
	func testGPGSAWithValidSentence() {
        if let instance = NMEASentenceParser.shared.parse(correctGPGSASentence) as? NMEASentenceParser.GPGSA {
            XCTAssertEqual(instance.fixSelectionMode, .auto)
            XCTAssertEqual(instance.threeDFixMode, .threed)
            XCTAssertEqual(instance.prn[0], 4)
            XCTAssertEqual(instance.prn[1], 5)
            XCTAssertEqual(instance.prn[2], nil)
            XCTAssertEqual(instance.prn[3], 9)
            XCTAssertEqual(instance.prn[4], 12)
            XCTAssertEqual(instance.prn[5], nil)
            XCTAssertEqual(instance.prn[6], nil)
            XCTAssertEqual(instance.prn[7], 24)
            XCTAssertEqual(instance.prn[8], nil)
            XCTAssertEqual(instance.prn[9], nil)
            XCTAssertEqual(instance.prn[10], nil)
            XCTAssertEqual(instance.prn[11], nil)
            XCTAssertEqual(instance.pdop, 2.5)
            XCTAssertEqual(instance.hdop, 1.3)
            XCTAssertEqual(instance.vdop, 2.1)
        } else {
            XCTFail("Failed casting to NMEASentenceParser.GPGSA")
        }
	}

    func testGlobalPerformance() {
        if let path = Bundle(for: type(of: self)).path(forResource: "NMEATestSentences", ofType: "txt") {
            do {
                var numberOfSentences = 0
                let data = try String(contentsOfFile: path, encoding: .utf8)
                let sentences = data.components(separatedBy: .newlines)
                self.measure {
                    for sentence in sentences {
                        _ = NMEASentenceParser.shared.parse(sentence)
                        numberOfSentences += 1
                    }
                }
                print("Parsed \(numberOfSentences) sentences")
            } catch {
                XCTFail(error.localizedDescription)
            }
        }
    }
}
